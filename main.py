#Author: Sarvesh Mangla

############################################################
#packages
import numpy as np
from scipy.optimize import curve_fit 
import sys 
import matplotlib.pyplot as plt 
#############################################################

print("I have entered the example data from VLA Calibrator Manual of 3C48")
print("http://www.vla.nrao.edu/astro/calib/manual/csource.html")
print("Copy and Paste the Data exactly the way it is given in the example for smooth run")
print("")
band_w = np.loadtxt("example.txt",dtype=float,comments='cm',skiprows=5,usecols=0)
flux = np.loadtxt("example.txt",dtype=float,skiprows=5,usecols=6)
band_f = 3.e10/band_w

ques=int(input("Want value at one frequency(Enter 0) or range in frequency(Enter 1) > "))

if ques==0:
    given_freq = float(input("Enter the Frequency in MHz > "))*1.e6
elif ques==1:
    print("I will be taking 1000 points between starting and ending of your range")
    S_freq = float(input("Enter the starting freq in MHz > "))
    F_freq = float(input("Enter the finishing freq in MHz> "))
    given_freq = np.linspace(S_freq,F_freq,1000)*1.e6
else:
    print("Entered the wrong input, TRY AGAIN")
    sys.exit()

def test(x, a, b):
    return -a*x+b 

y=np.log(flux)
x=np.log(band_f)

param, param_cov = curve_fit(test, x, y)

ans=np.exp(param[1])*np.power(given_freq,-param[0])
if ques==0:
    print("Flux value at "+str(given_freq/1e6)+"MHz is "+str(np.round(ans,2))+"Jy")
else:
    plt.plot(given_freq,ans,'k-')
    plt.xscale('log')
    plt.xlabel('Freq (Hz)')
    plt.ylabel('Flux (Jy)')
    plt.show()
# GMRT-VLA calibrators

VLA Calibrator manual: http://www.vla.nrao.edu/astro/calib/manual/csource.html

The calibrator data (for which, you want to run) should be replaced in the example.txt file and then run the main.py file in python

The final answer are just an estimate computed using curve fitting and they can be vary from the actual answer.
